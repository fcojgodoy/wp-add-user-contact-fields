var gulp = require( 'gulp' );
var wpPot = require( 'gulp-wp-pot' );

gulp.task( 'pot', function () {
    return gulp.src( '**/*.php' )
        .pipe( wpPot( {
            domain: 'wp-add-user-contact-fields',
            package: 'WP Add User Contact Fields'
        } ) )
        .pipe( gulp.dest( './languages/wp-add-user-contact-fields.pot' ) );
} );